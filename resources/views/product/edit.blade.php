@extends('layouts.app')

@section('content')
<div class="container">
  <div class="container">
    <h2>Edit Product</h2><br/>
    <form method="post" action="{{action('ProductController@update', $id)}}" enctype="multipart/form-data">
      @csrf
      <input name="_method" type="hidden" value="PATCH">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <label for="Name">Name:</label>
          <input type="text" class="form-control" name="name" value="{{$product->name}}" required>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Email">quantity:</label>
            <input type="text" class="form-control" name="quantity"  value="{{$product->quantity}}" required>
          </div>
        </div>
       <div class="row">
        <div class="col-md-4"></div>
          <div class="form-group col-md-4">
              <lable>Category</lable>
              <select name="category_id" required>
                @foreach($categories as $category)
                  <option value="{{$category['id']}}"  @if($product->category_id==$category['id']) selected @endif>{{$category['title']}}</option>
                 @endforeach
              </select>
          </div>
      </div>
      <div class="row">
       <div class="col-md-4"></div>
       <div class="form-group col-md-4" style="margin-top:60px">
         <button type="submit" class="btn btn-success">Submit</button>
       </div>
     </div>
    </form>
    </div>
</div>
@endsection
