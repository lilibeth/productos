@extends('layouts.app')

@section('content')
<div class="container">
  <div class="container">
    <h2>Edit category</h2><br/>
    <form method="post" action="{{action('CategoryController@update', $id)}}" enctype="multipart/form-data">
      @csrf
      <input name="_method" type="hidden" value="PATCH">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <label for="Name">Name:</label>
          <input type="text" class="form-control" name="title" value="{{$category->title}}" required>
        </div>
      </div>
      <div class="row">
       <div class="col-md-4"></div>
       <div class="form-group col-md-4" style="margin-top:60px">
         <button type="submit" class="btn btn-success">Submit</button>
       </div>
     </div>
    </form>
    </div>
</div>
@endsection
