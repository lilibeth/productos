@extends('layouts.app')

@section('content')
<div class="container">
  <div class="container">
       <h2>Create Category</h2><br/>
       <form method="post" action="{{url('category')}}" enctype="multipart/form-data">
         @csrf
         <div class="row">
           <div class="col-md-4"></div>
           <div class="form-group col-md-4">
             <label for="Name">Name:</label>
             <input type="text" class="form-control" name="title" required>
           </div>
         </div>
         <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
       </form>
     </div>

 </div>
 </div>
</div>
@endsection
