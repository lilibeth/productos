<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
  public function index()
    {
        $products=\App\Products::all();
        return view('product.index',compact('products'));
    }
  public function create()
  {
      $categories=\App\Categories::all();
      return view("product.create", compact('categories'));
  }
  public function store(Request $request)
  {
   $product = new \App\Products;

   $product->name = \Request::input('name');
   $product->quantity = \Request::input('quantity');
   $product->category_id = \Request::input('category_id');
   $product->save();

   return redirect('product')->with('success', 'Product saved');

  }
  public function edit($id)
   {
       $product = \App\Products::find($id);
       $categories=\App\Categories::all();
       return view('product.edit',compact('product','id', 'categories'));
   }
   public function update(Request $request, $id)
   {
       $product= \App\Products::find($id);
       $product->name = \Request::input('name');
       $product->quantity = \Request::input('quantity');
       $product->category_id = \Request::input('category_id');
       $product->save();
       return redirect('product');
   }

   public function destroy($id)
    {
        $product = \App\Products::find($id);
        $product->delete();
        return redirect('product')->with('success','Information has been  deleted');
    }
}
