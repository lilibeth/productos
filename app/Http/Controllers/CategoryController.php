<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class CategoryController extends Controller
{
  public function index()
    {
        $categories=\App\Categories::all();
        return view('category.index',compact('categories'));
    }
  public function create()
  {
      return view("category.create");
  }

  public function store(Request $request)
  {
   $category = new \App\Categories;

   $category->title = \Request::input('title');

   $category->save();

   return redirect('category')->with('success', 'category saved');
  }
  public function edit($id)
   {
       $category = \App\Categories::find($id);
       return view('category.edit',compact('category','id'));
   }
   public function update(Request $request, $id)
   {
       $category= \App\Categories::find($id);
       $category->title = \Request::input('title');
       $category->save();
       return redirect('category');
   }

   public function destroy($id)
    {
        $category = \App\Categories::find($id);
        $category->delete();
        return redirect('category')->with('success','Information has been  deleted');
    }
}
